package ru.nsu.fit.endpoint.service.database.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.UUID;

import static org.junit.Assert.*;

/**
 * Created by tolyas on 9/29/16.
 */
public class ServicePlanTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewServicePlan() {
        new ServicePlan(0, "Name", "Details", 1000, 10, 200);
    }

    @Test
    public void testCreateNewServicePlanVeryLongName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Name's length should be more or equal 2 symbols and less or equal 128 symbols");
        new ServicePlan(0, "NameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameName1", "Details", 1000, 10, 200);
    }

    @Test
    public void testCreateNewServicePlanVeryName() {
        new ServicePlan(0, "NameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameName", "Details", 1000, 10, 200);
    }

    @Test
    public void testCreateNewServicePlanVeryShortName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Name's length should be more or equal 2 symbols and less or equal 128 symbols");
        new ServicePlan(0, "a", "Details", 1000, 10, 200);
    }

    @Test
    public void testCreateNewServicePlanShortName() {
        new ServicePlan(0, "zx", "Details", 1000, 10, 200);
    }

    @Test
    public void testCreateNewServicePlanInvalidName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Name's length shouldn't contain special characters");
        new ServicePlan(0, "Nam@e", "Details", 1000, 10, 200);
    }

    @Test
    public void testCreateNewServicePlanVeryShortDetails() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Details's length should be more or equal 1 symbols and less or equal 1024 symbols");
        new ServicePlan(0, "Name", "", 1000, 10, 200);
    }

    @Test
    public void testCreateNewServicePlanShortDetails() {
        new ServicePlan(0, "Name", "a", 1000, 10, 200);
    }

    @Test
    public void testCreateNewServicePlanVeryLongDetails() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Details's length should be more or equal 1 symbols and less or equal 1024 symbols");
        new ServicePlan(0, "Name", "NameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameName1", 1000, 10, 200);
    }

    @Test
    public void testCreateNewServicePlanLongDetails() {
        new ServicePlan(0, "Name", "NameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameNameName", 1000, 10, 200);
    }

    @Test
    public void testCreateNewServicePlanDetails() {
        new ServicePlan(0, "Name", "Details Det@_ails", 1000, 10, 200);
    }

    @Test
    public void testCreateNewServicePlanMaxSeatsSmall() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Max seats should be more or equal 1 and less or equal 999999");
        new ServicePlan(0, "Name", "Details", 0, 10, 200);
    }

    @Test
    public void testCreateNewServicePlanMaxSeatsMinimum() {
        new ServicePlan(0, "Name", "Details", 1, 1, 200);
    }

    @Test
    public void testCreateNewServicePlanMaxSeatsBig() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Max seats should be more or equal 1 and less or equal 999999");
        new ServicePlan(0, "Name", "Details", 1000000, 10, 200);
    }

    @Test
    public void testCreateNewServicePlanMaxSeatsMaximum() {
        new ServicePlan(0, "Name", "Details", 999999, 10, 200);
    }

    @Test
    public void testCreateNewServicePlanMinSeatsSmall() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Min seats should be more or equal 1 and less or equal 999999");
        new ServicePlan(0, "Name", "Details", 1000, 0, 200);
    }

    @Test
    public void testCreateNewServicePlanMinSeatsMinimum() {
        new ServicePlan(0, "Name", "Details", 1000, 1, 200);
    }

    @Test
    public void testCreateNewServicePlanMinSeatsBig() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Min seats should be more or equal 1 and less or equal 999999");
        new ServicePlan(0, "Name", "Details", 1000, 1000000, 200);
    }

    @Test
    public void testCreateNewServicePlanMinSeatsMaximum() {
        new ServicePlan(0, "Name", "Details", 999999, 999999, 200);
    }

    @Test
    public void testCreateNewServicePlanMinSeatsMoreMax() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Max should be more or equal min");
        new ServicePlan(0, "Name", "Details", 1000, 100000, 200);
    }

    @Test
    public void testCreateNewServicePlanMinSeatsEqualMax() {
        new ServicePlan(0, "Name", "Details", 1000, 1000, 200);
    }

    @Test
    public void testCreateNewServicePlanFeePerUnitVerySmall() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Fee per unit should be more or equal 0 and less or equal 999999");
        new ServicePlan(0, "Name", "Details", 100, 10, -1);
    }

    @Test
    public void testCreateNewServicePlanFeePerUnitSmall() {
        new ServicePlan(0, "Name", "Details", 100, 10, 0);
    }

    @Test
    public void testCreateNewServicePlanFeePerUnitBig() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Fee per unit should be more or equal 0 and less or equal 999999");
        new ServicePlan(0, "Name", "Details", 100, 10, 1000000);
    }

    @Test
    public void testCreateNewServicePlanFeePerUnitMaximum() {
        new ServicePlan(0, "Name", "Details", 100, 10, 999999);
    }

}