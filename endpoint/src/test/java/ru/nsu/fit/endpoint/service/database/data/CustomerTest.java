package ru.nsu.fit.endpoint.service.database.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import ru.nsu.fit.endpoint.service.database.data.Customer;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class CustomerTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewCustomer() {
        new Customer("John", "Wick", "john_wick@gmail.com", "str0Ngp@ss", 0);
    }

    @Test
    public void testCreateNewCustomerWithVeryShortPass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password's length should be more or equal 6 symbols and less or equal 12 symbols");
        new Customer("John", "Wick", "john_wick@gmail.com", "12#45", 0);
    }

    @Test
    public void testCreateNewCustomerWithShortPass() {
        new Customer("John", "Wick", "john_wick@gmail.com", "123#56", 0);
    }

    @Test
    public void testCreateNewCustomerWithVeryLongPass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password's length should be more or equal 6 symbols and less or equal 12 symbols");
        new Customer("John", "Wick", "john_wick@gmail.com", "str0Ngp@sswe1", 0);
    }

    @Test
    public void testCreateNewCustomerWithLongPass() {
        new Customer("John", "Wick", "john_wick@gmail.com", "str0Ngp@sswe", 0);
    }

    @Test
    public void testCreateNewCustomerWithEmptyPass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password's length should be more or equal 6 symbols and less or equal 12 symbols");
        new Customer("John", "Wick", "john_wick@gmail.com", "", 0);
    }

    @Test
    public void testCreateNewCustomerWithEasyPass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password is easy");
        new Customer("John", "Wick", "john_wick@gmail.com", "123qwe", 0);
    }

    @Test
    public void testCreateNewCustomerWithLoginPass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password contains login");
        new Customer("John", "Wick", "joh@gml.cm", "1joh@gml.cme", 0);
    }

    @Test
    public void testCreateNewCustomerWithFirstNamePass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password contains first name");
        new Customer("John", "Wick", "joh@gml.cm", "1Johne", 0);
    }

    @Test
    public void testCreateNewCustomerWithSecondNamePass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password contains last name");
        new Customer("John", "Wick", "joh@gml.cm", "1Wicke", 0);
    }

    @Test
    public void testCreateNewCustomerWithNegativeMoney(){
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Money mist be positive value");
        new Customer("John", "Wick", "john_wick@gmail.com", "dsfsdfjfnsdl", -1);
    }

    @Test
    public void testCreateNewCustomerShortFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("First name should be more or equal 2 symbols and less or equal 12 symbols");
        new Customer("p", "Wick", "john_wick@gmail.com", "dsfsdfjfnsdl", 0);
    }

    @Test
    public void testCreateNewCustomerLongFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("First name should be more or equal 2 symbols and less or equal 12 symbols");
        new Customer("JohnJohnJohn1", "Wick", "john_wick@gmail.com", "dsfsdfjfnsdl", 0);
    }

    @Test
    public void testCreateNewCustomerShortSecondName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Second name should be more or equal 2 symbols and less or equal 12 symbols");
        new Customer("John", "W", "john_wick@gmail.com", "dsfsdfjfnsdl", 0);
    }

    @Test
    public void testCreateNewCustomerLongSecondName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Second name should be more or equal 2 symbols and less or equal 12 symbols");
        new Customer("John", "WickWickWick1", "john_wick@gmail.com", "dsfsdfjfnsdl", 0);
    }

    @Test
    public void testCreateNewCustomerInvalidLoginWithoutAt() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Invalid email");
        new Customer("John", "Wick", "john_wickgmail.com", "dsfsdfjfnsdl", 0);
    }

    @Test
    public void testCreateNewCustomerInvalidLoginWithManyAts() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Invalid email");
        new Customer("John", "Wick", "john_wick@gm@ail.com", "dsfsdfjfnsdl", 0);
    }

    @Test
    public void testCreateNewCustomerInvalidLoginWithShortDomain() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Invalid email");
        new Customer("John", "Wick", "john_wick@gm.c", "dsfsdfjfnsdl", 0);
    }

    @Test
    public void testCreateNewCustomerInvalidLoginDotEnding() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Invalid email");
        new Customer("John", "Wick", "john_wick@gmail.com.", "dsfsdfjfnsdl", 0);
    }

    @Test
    public void testCreateNewCustomerInvalidLoginWithoutDomain() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Invalid email");
        new Customer("John", "Wick", "john_wick@gmail", "dsfsdfjfnsdl", 0);
    }

    @Test
    public void testCreateNewCustomerWithInvalidFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("First name should start with capital letter not contain numbers and special characters!");
        new Customer("Jo1hn", "Wick", "john_wick@gmail.com", "dsfsdfjfnsdl", 0);
    }

    @Test
    public void testCreateNewCustomerWithInvalidLastName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Last name should start with capital letter not contain numbers and special characters!");
        new Customer("John", "Wi1ck", "john_wick@gmail.com", "dsfsdfjfnsdl", 0);
    }
}
