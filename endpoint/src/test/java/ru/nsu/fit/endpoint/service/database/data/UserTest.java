package ru.nsu.fit.endpoint.service.database.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

/**
 * Created by tolyas on 9/29/16.
 */
public class UserTest {
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewUser() {
        new User("John", "Wick", "Login", "john_wick@gmail.com", "strongpass", User.UserRole.USER);
    }
    
    @Test
    public void testCreateNewUserWithShortPass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password's length should be more or equal 6 symbols and less or equal 12 symbols");
        new User("John", "Wick", "Login", "john_wick@gmail.com", "12312", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithLongPass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password's length should be more or equal 6 symbols and less or equal 12 symbols");
        new User("John", "Wick", "Login", "john_wick@gmail.com", "123qwe123qwe1", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithEasyPass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password is easy");
        new User("John", "Wick", "Login", "john_wick@gmail.com", "123qwe", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithLoginPass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password contains login");
        new User("John", "Wick", "Login", "joh@gml.cm", "1Logine", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithFirstNamePass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password contains first name");
        new User("John", "Wick", "Login", "joh@gml.cm", "1Johne", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserWithSecondNamePass() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Password contains last name");
        new User("John", "Wick", "Login", "joh@gml.cm", "1Wicke", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserVeryShortFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("First name should be more or equal 2 symbols and less or equal 12 symbols");
        new User("p", "Wick", "Login", "john_wick@gmail.com", "dsfsdfjfnsdl", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserShortFirstName() {
        new User("Jo", "Wick", "Login", "john_wick@gmail.com", "dsfsdfjfnsdl", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserVeryLongFirstName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("First name should be more or equal 2 symbols and less or equal 12 symbols");
        new User("JohnJohnJohn1", "Wick", "Login", "john_wick@gmail.com", "dsfsdfjfnsdl", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserLongFirstName() {
        new User("JohnJohnJohn", "Wick", "Login", "john_wick@gmail.com", "dsfsdfjfnsdl", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserVeryShortSecondName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Second name should be more or equal 2 symbols and less or equal 12 symbols");
        new User("John", "W", "Login", "john_wick@gmail.com", "dsfsdfjfnsdl", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserShortSecondName() {
        new User("John", "Wr", "Login", "john_wick@gmail.com", "dsfsdfjfnsdl", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserVeryLongSecondName() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Second name should be more or equal 2 symbols and less or equal 12 symbols");
        new User("John", "WickWickWick1", "Login", "john_wick@gmail.com", "dsfsdfjfnsdl", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserLongSecondName() {
        new User("John", "WickWickWick", "Login", "john_wick@gmail.com", "dsfsdfjfnsdl", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserVeryShortLogin() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Login should be more or equal 2 symbols and less or equal 12 symbols");
        new User("John", "Wick", "m", "john_wick@gmail.com", "dsfsdfjfnsdl", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserShortLogin() {
        new User("John", "Wick", "Lo", "john_wick@gmail.com", "dsfsdfjfnsdl", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserVeryLongLogin() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Login should be more or equal 2 symbols and less or equal 12 symbols");
        new User("John", "Wick", "LoginLogin123", "john_wick@gmail.com", "dsfsdfjfnsdl", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserLongLogin() {
        new User("John", "Wick", "LoginLogin12", "john_wick@gmail.com", "dsfsdfjfnsdl", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserInvalidEmailWithoutAt() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Invalid email");
        new User("John", "Wick", "Login", "john_wickgmail.com", "dsfsdfjfnsdl", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserInvalidEmailWithTwoAts() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Invalid email");
        new User("John", "Wick", "Login", "john_wick@gm@ail.com", "dsfsdfjfnsdl", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserInvalidEmailWithShortDomain() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Invalid email");
        new User("John", "Wick", "Login", "john_wick@gm.c", "dsfsdfjfnsdl", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserInvalidEmailDotEnding() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Invalid email");
        new User("John", "Wick", "Login", "john_wick@gmail.com.", "dsfsdfjfnsdl", User.UserRole.USER);
    }

    @Test
    public void testCreateNewUserInvalidEmailWithoutDomain() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Invalid email");
        new User("John", "Wick", "Login", "john_wick@gmail", "dsfsdfjfnsdl", User.UserRole.USER);
    }
}