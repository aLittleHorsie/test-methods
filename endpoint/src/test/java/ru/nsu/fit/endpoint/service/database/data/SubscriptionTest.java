package ru.nsu.fit.endpoint.service.database.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

/**
 * Created by tolyas on 9/29/16.
 */
public class SubscriptionTest {

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testCreateNewSubscription() {
        new Subscription(1000, 10, 200);
    }

    @Test
    public void testCreateNewSubscriptionMaxSeatsSmall() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Max seats should be more or equal 1 and less or equal 999999");
        new Subscription(0, 10, 200);
    }

    @Test
    public void testCreateNewSubscriptionMaxSeatsMinimum() {
        new Subscription(1, 1, 1);
    }

    @Test
    public void testCreateNewSubscriptionMaxSeatsBig() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Max seats should be more or equal 1 and less or equal 999999");
        new Subscription(1000000, 10, 200);
    }

    @Test
    public void testCreateNewSubscriptionMaxSeatsMaximum() {
        new Subscription(999999, 10, 200);
    }

    @Test
    public void testCreateNewSubscriptionMinSeatsSmall() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Min seats should be more or equal 1 and less or equal 999999");
        new Subscription(1000, 0, 200);
    }

    @Test
    public void testCreateNewSubscriptionMinSeatsMinimum() {
        new Subscription(1000, 1, 200);
    }

    @Test
    public void testCreateNewSubscriptionMinSeatsBig() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Min seats should be more or equal 1 and less or equal 999999");
        new Subscription(1000, 1000000, 200);
    }

    @Test
    public void testCreateNewSubscriptionMinSeatsMaximum() {
        new Subscription(999999, 999999, 999999);
    }

    @Test
    public void testCreateNewSubscriptionMinSeatsMoreMax() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Max should be more or equal min");
        new Subscription(1000, 100000, 200);
    }

    @Test
    public void testCreateNewSubscriptionMinSeatsEqualMax() {
        new Subscription(1000, 1000, 1000);
    }

    @Test
    public void testCreateNewSubscriptionUsedSeatsMoreMax() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Used should be more or equal min and less or equal than max");
        new Subscription(1000, 100, 1001);
    }

    @Test
    public void testCreateNewSubscriptionUsedSeatsEqualMax() {
        new Subscription(1000, 100, 1000);
    }

    @Test
    public void testCreateNewSubscriptionUsedSeatsLessMin() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Used should be more or equal min and less or equal than max");
        new Subscription(1000, 100, 99);
    }

    @Test
    public void testCreateNewSubscriptionUsedSeatsEqualMin() {
        new Subscription(1000, 100, 100);
    }

    @Test
    public void testCreateNewSubscriptionUsedSeatsVerySmall() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Used seats should be more or equal 1 and less or equal 999999");
        new Subscription(1000, 1, 0);
    }

    @Test
    public void testCreateNewSubscriptionUsedSeatsSmall() {
        new Subscription(1000, 1, 1);
    }

    @Test
    public void testCreateNewSubscriptionUsedSeatsBig() {
        new Subscription(999999, 100, 999999);
    }

    @Test
    public void testCreateNewSubscriptionUsedSeatsVeryBig() {
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Used seats should be more or equal 1 and less or equal 999999");
        new Subscription(1000, 100, 1000000);
    }

}