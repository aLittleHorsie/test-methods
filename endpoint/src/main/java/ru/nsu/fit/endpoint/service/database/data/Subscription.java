package ru.nsu.fit.endpoint.service.database.data;

import org.apache.commons.lang.Validate;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class Subscription {
    private UUID id;
    private UUID customerId;
    private UUID servicePlanId;
    private int maxSeats;
    private int minSeats;
    private int usedSeats;

    public Subscription(int maxSeats, int minSeats, int usedSeats) {
        this.id = UUID.randomUUID();
        this.customerId = UUID.randomUUID();
        this.servicePlanId = UUID.randomUUID();
        this.maxSeats = maxSeats;
        this.minSeats = minSeats;
        this.usedSeats = usedSeats;
        validate(maxSeats,minSeats,usedSeats);
    }

    public static void validate(int maxSeats, int minSeats, int usedSeats){
        Validate.isTrue(maxSeats >= 1 && maxSeats <= 999999, "Max seats should be more or equal 1 and less or equal 999999");
        Validate.isTrue(minSeats >= 1 && minSeats <= 999999, "Min seats should be more or equal 1 and less or equal 999999");
        Validate.isTrue(minSeats <= maxSeats, "Max should be more or equal min");
        Validate.isTrue(usedSeats >= 1 && usedSeats <= 999999, "Used seats should be more or equal 1 and less or equal 999999");
        Validate.isTrue(usedSeats <= maxSeats && usedSeats >= minSeats, "Used should be more or equal min and less or equal than max");
    }
}
