package ru.nsu.fit.endpoint.service.database.data;

import org.apache.commons.lang.Validate;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class ServicePlan {
    //private UUID id;
    private int id;
    /* Длина не больше 128 символов и не меньше 2 включительно не содержит спец символов */
    private String name;
    /* Длина не больше 1024 символов и не меньше 1 включительно */
    private String details;
    /* Не больше 999999 и не меньше 1 включительно */
    private int maxSeats;
    /* Не больше 999999 и не меньше 1 включительно, minSeats >= maxSeats */
    private int minSeats;
    /* Больше ли равно 0 но меньше либо равно 999999 */
    private int feePerUnit;

    public ServicePlan(int id, String name, String details, int maxSeats, int minSeats, int feePerUnit) {
        //this.id = UUID.randomUUID();
        this.id = id;
        this.name = name;
        this.details = details;
        this.maxSeats = maxSeats;
        this.minSeats = minSeats;
        this.feePerUnit = feePerUnit;
        validate(name, details, maxSeats, minSeats, feePerUnit);
    }

    public static void validate(String name, String details, int maxSeats, int minSeats, int feePerUnit){
        Validate.isTrue(name.length() >= 2 && name.length() <= 128, "Name's length should be more or equal 2 symbols and less or equal 128 symbols");
        Validate.isTrue(name.matches("^[A-Za-z0-9\\s.,]*$"), "Name's length shouldn't contain special characters");
        Validate.isTrue(details.length() >= 1 && details.length() <= 1024, "Details's length should be more or equal 1 symbols and less or equal 1024 symbols");
        Validate.isTrue(maxSeats >= 1 && maxSeats <= 999999, "Max seats should be more or equal 1 and less or equal 999999");
        Validate.isTrue(minSeats >= 1 && minSeats <= 999999, "Min seats should be more or equal 1 and less or equal 999999");
        Validate.isTrue(minSeats <= maxSeats, "Max should be more or equal min");
        Validate.isTrue(feePerUnit >= 0 && feePerUnit <= 999999, "Fee per unit should be more or equal 0 and less or equal 999999");
    }

    public int getFeePerUnit() {
        return feePerUnit;
    }

    public void setFeePerUnit(int feePerUnit) {
        this.feePerUnit = feePerUnit;
    }

    public int getMaxSeats() {
        return maxSeats;
    }

    public void setMaxSeats(int maxSeats) {
        this.maxSeats = maxSeats;
    }

    public int getMinSeats() {
        return minSeats;
    }

    public void setMinSeats(int minSeats) {
        this.minSeats = minSeats;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
