DROP TABLE IF EXISTS Subscription, Finance, User, ServicePlan, UserRole;

CREATE TABLE UserRole (
  ID INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  Name VARCHAR(50) NOT NULL
);

CREATE TABLE ServicePlan (
  ID INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  Name VARCHAR(50) NOT NULL,
  Details VARCHAR(1000) NOT NULL,
  Seats INT(6) UNSIGNED NOT NULL,
  FeePerUnit INT(6) UNSIGNED NOT NULL
);

CREATE TABLE User (
  ID INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  FirstName VARCHAR(30) NOT NULL,
  LastName VARCHAR(30) NOT NULL,
  Email VARCHAR(30) NOT NULL UNIQUE KEY,
  Login VARCHAR(30) NOT NULL UNIQUE KEY,
  Password VARCHAR(30) NOT NULL,
  UserRoleID INT(6) UNSIGNED NOT NULL,
  FOREIGN KEY (UserRoleID) REFERENCES UserRole(ID) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE Finance (
  ID INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  MoneyAmount INT(6) UNSIGNED NOT NULL,
  TransactionTime TIMESTAMP,
  UserID INT(6) UNSIGNED NOT NULL,
  FOREIGN KEY (UserID) REFERENCES User(ID) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE Subscription (
  ID INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  UserID INT(6) UNSIGNED NOT NULL,
  ServicePlanID INT(6) UNSIGNED NOT NULL,
  FOREIGN KEY (UserID) REFERENCES User(ID) ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (ServicePlanID) REFERENCES ServicePlan(ID) ON UPDATE CASCADE ON DELETE CASCADE

);

INSERT INTO UserRole (Name) values ('Company administrator');
INSERT INTO UserRole (Name) values ('Technical administrator');
INSERT INTO UserRole (Name) values ('Billing administrator');
INSERT INTO UserRole (Name) values ('User');


INSERT INTO User (FirstName, LastName, Email, Login, Password, UserRoleID) values
  ('Alex', 'Roodman', 'galaxy@gmail.com', 'lol', 'lol', 4);

INSERT INTO ServicePlan (Name, Details, Seats, FeePerUnit) VALUES ('Morning sandwiches with delivery.',
                                'The best sandwiches in the world with ultimate delivery to your mouth!',20,10),
('Any movie, any time','Watch anything you want. Without any restrictions!',10,2500),
('Compliments on the phone','Unlimited compliments just for you! Anytime you want!',4500,1),
('Programming','We\'ll program whatever you want. Unlimited rows of code. Additional cookies required.',5,2000);