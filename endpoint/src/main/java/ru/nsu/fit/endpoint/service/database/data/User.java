package ru.nsu.fit.endpoint.service.database.data;

import org.apache.commons.lang.Validate;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class User {
    private UUID customerId;
    private UUID[] subscriptionIds;
    private UUID id;
    /* нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов */
    private String firstName;
    private String lastName;
    private String login;
    /* указывается в виде email, проверить email на корректность */
    private String email;
    /* длина от 6 до 12 символов включительно, недолжен быть простым, не должен содержать части login, firstName, lastName */
    private String pass;
    private UserRole userRole;

    public User(String firstName, String lastName, String login, String email, String pass, UserRole userRole) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.login = login;
        this.email = email;
        this.pass = pass;
        this.userRole = userRole;
        validate(firstName,lastName,login,email,pass,userRole);
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public UUID[] getSubscriptionIds() {
        return subscriptionIds;
    }

    public void setSubscriptionIds(UUID[] subscriptionIds) {
        this.subscriptionIds = subscriptionIds;
    }

    public UUID getCustomerId() {
        return customerId;
    }

    public void setCustomerId(UUID customerId) {
        this.customerId = customerId;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public static enum UserRole {
        COMPANY_ADMINISTRATOR("1"),
        TECHNICAL_ADMINISTRATOR("2"),
        BILLING_ADMINISTRATOR("3"),
        USER("4");

        private String roleName;

        UserRole(String roleName) {
            this.roleName = roleName;
        }

        public String getRoleName() {
            return roleName;
        }
    }

    public static void validate(String firstName, String lastName, String login, String email, String pass, UserRole userRole){
        Validate.notNull(pass);
        Validate.isTrue(pass.length() >= 6 && pass.length() < 13, "Password's length should be more or equal 6 symbols and less or equal 12 symbols");
        Validate.isTrue(!pass.matches("^[a-z|0-9]{6}$"), "Password is easy");
        Validate.isTrue(!pass.toLowerCase().contains(login.toLowerCase()), "Password contains login");
        Validate.isTrue(!pass.toLowerCase().contains(firstName.toLowerCase()), "Password contains first name");
        Validate.isTrue(!pass.toLowerCase().contains(lastName.toLowerCase()), "Password contains last name");
        Validate.isTrue(firstName.length() >= 2 && firstName.length() <= 12, "First name should be more or equal 2 symbols and less or equal 12 symbols");
        Validate.isTrue(lastName.length() >= 2 && lastName.length() <= 12, "Second name should be more or equal 2 symbols and less or equal 12 symbols");
        Validate.isTrue(login.length() >= 2 && login.length() <= 12, "Login should be more or equal 2 symbols and less or equal 12 symbols");
        Validate.isTrue(email.matches("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$"), "Invalid email");
    }
}
