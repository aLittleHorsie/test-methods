package ru.nsu.fit.endpoint;

/**
 * Created by aleksandrkucherov on 02.10.16.
 */
public class Style {
    public static String Bootstrap = "<link rel=\"stylesheet\" " +
            "href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" " +
            "integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" " +
            "crossorigin=\"anonymous\">";

    public static String Js = "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js\"></script>";
}
