package ru.nsu.fit.endpoint;

/**
 * Created by aleksandrkucherov on 29.09.16.
 */

import ru.nsu.fit.endpoint.service.database.data.ServicePlan;
import java.security.Provider;
import java.sql.*;
import java.util.ArrayList;

public class db_connect {
    static Connection Conn = null;
    static PreparedStatement PrepareStat = null;

    public static void DBadd(String FName, String LName, String Email, String Login, String Pass) {

        try {
            log("-------- Connecting to DB ------------");
            makeJDBCConnection();

            log("\n---------- Adding Data to DB ----------");
            addUserToDB(FName, LName, Email, Login, Pass, 4);

            PrepareStat.close();
            //Conn.close(); // connection close

        } catch (SQLException e) {

            e.printStackTrace();
        }
    }

    public static Boolean DBcheckREG(String Login, String Pass) {

        try {

            log("-------- Connecting to DB ------------");
            makeJDBCConnection();

            log("\n---------- Checking User in DB ----------");
            if (CheckUserFromDB(Login, Pass)){
                PrepareStat.close();
                //Conn.close(); // connection close
                log("\nUser exists");
                return Boolean.TRUE;

            } else return Boolean.FALSE;


        } catch (SQLException e) {

            e.printStackTrace();
            return Boolean.FALSE;

        }
    }

    public static ArrayList<ServicePlan> checkServList() {

        try {

            log("-------- Connecting to DB ------------");
            makeJDBCConnection();

            log("\n---------- Getting service plans from DB ----------");
            PrepareStat.close();
            //Conn.close();

            log ("Successfully received services from DB!");
            return GetServicePlanFromDB();

        } catch (SQLException e) {

            e.printStackTrace();
            return null;

        }


    }

    private static void makeJDBCConnection() {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            log("Congrats - Seems your MySQL JDBC Driver Registered!");
        } catch (ClassNotFoundException e) {
            log("Sorry, couldn't found JDBC driver. Make sure you have added JDBC Maven Dependency Correctly");
            e.printStackTrace();
            return;
        }

        try {
            // DriverManager: The basic service for managing a set of JDBC drivers.
            Conn = DriverManager.getConnection("jdbc:mysql://localhost:8889/JavaTest","root", "root");
            if (Conn != null) {
                log("Connection Successful! Enjoy. Now it's time to push data");
            } else {
                log("Failed to make connection!");
            }
        } catch (SQLException e) {
            log("MySQL Connection Failed!");
            e.printStackTrace();
            return;
        }

    }

    private static void addUserToDB(String FirstName, String LastName, String Email,
                                    String Login, String Password, int UserRole) {

        try {
            String insertQueryStatement = "INSERT  INTO  User (FirstName, LastName, Email, Login, Password, UserRoleID)  VALUES  (?,?,?," +
                    "?,?,?)";

            PrepareStat = Conn.prepareStatement(insertQueryStatement);
            PrepareStat.setString(1, FirstName);
            PrepareStat.setString(2, LastName);
            PrepareStat.setString(3, Email);
            PrepareStat.setString(4, Login);
            PrepareStat.setString(5, Password);
            PrepareStat.setInt(6, UserRole);


            // execute insert SQL statement
            PrepareStat.executeUpdate();
            log(FirstName + " added successfully");
        } catch (

                SQLException e) {
            e.printStackTrace();
        }
    }

    private static Boolean CheckUserFromDB(String Login, String Password) {

        try {

            String getQueryStatement = "SELECT * FROM User WHERE Login='" + Login +"' and Password='" + Password + "';";
            PrepareStat = Conn.prepareStatement(getQueryStatement);
            ResultSet rs = PrepareStat.executeQuery();

            //  while (rs.next()) {
            if (rs.next()){
                return Boolean.TRUE;
            } else return Boolean.FALSE;
            //  }

        } catch (

                SQLException e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }

    }

    private static ArrayList<ServicePlan> GetServicePlanFromDB() {



        try {

            String getQueryStatement = "SELECT * FROM ServicePlan";
            PrepareStat = Conn.prepareStatement(getQueryStatement);
            ResultSet rs = PrepareStat.executeQuery();

                ArrayList servPlist = new ArrayList<ServicePlan>();
                while (rs.next()) {
                    ServicePlan Service = new ServicePlan(rs.getInt("Id"),rs.getString("Name"),rs.getString("details"),
                            rs.getInt("Seats"),1,rs.getInt("FeePerUnit"));
                    servPlist.add(Service);
                    System.out.println(Service.getName() +" || "+ Service.getDetails() +" || "+ Service.getFeePerUnit());
                } return servPlist;


        } catch (

                SQLException e) {
            e.printStackTrace();
            return null;
        }



    }

    private static int getUserMoneyAmount(int userId){

        try {

            String getQueryStatement = "SELECT SUM(MoneyAmount) sum FROM Finance WHERE UserID=" + userId;
            PrepareStat = Conn.prepareStatement(getQueryStatement);
            ResultSet rs = PrepareStat.executeQuery();


            if (rs.next()){
                return rs.getInt("sum");
            }

        } catch (

                SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    // Simple log utility
    private static void log(String string) {
        System.out.println(string);

    }
}