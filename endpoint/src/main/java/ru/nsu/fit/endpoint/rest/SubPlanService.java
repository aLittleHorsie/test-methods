package ru.nsu.fit.endpoint.rest;


import ru.nsu.fit.endpoint.db_connect;
import ru.nsu.fit.endpoint.Style;
import ru.nsu.fit.endpoint.service.database.data.ServicePlan;
import ru.nsu.fit.endpoint.service.database.data.User;

//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

/**
 * Created by aleksandrkucherov on 02.10.16.
 */

@Path("/service_plan")
public class SubPlanService {

   // @Context private HttpServletRequest request;
    @GET
    @Path("/list")
    public Response ServicePlans(
            @QueryParam("login") String login
    ) {

        String res = Style.Bootstrap;
        res += "<style>" +
                ".row{\n" +
                "            width: 80%;\n" +
                "            margin-top: 10%;\n" +
                "            margin-left: 10%;\n" +
                "            margin-right: 10%;\n" +
                "        }" +
                "</style>";
        res += "<div class=\"row\">";


        for (ServicePlan sp : db_connect.checkServList()) {

            res += "<div class=\"col-md-4\">";

            res += "<p class=\"ServPlan_Name\">";
            res += sp.getName();
            res += "</p>";
            res += "<p class=\"ServPlan_Details\">";
            res += sp.getDetails();
            res += "</p>";
            res += "<p class=\"ServPlan_Price\">";
            res += sp.getFeePerUnit() + "$";
            res += "</p>";

            res += "<p class=\"ServPlan_Subs\">";
            res += "<a href=\"http://localhost:8081/rest/subscription/subscribe?user=" + login + " &sub_id=" +
                    sp.getId() +
                    "\" class=\"btn btn-primary btn-lg active\" role=\"betton\">Subscribe</a>";

            res += "</p>";

            res += "</div>";

        }

        res += "</div>";
        res += Style.Js;

        return Response.status(200)
                .entity(res)
                .build();

    }
}
