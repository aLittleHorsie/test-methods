package ru.nsu.fit.endpoint.rest;

import ru.nsu.fit.endpoint.Style;
import ru.nsu.fit.endpoint.db_connect;
import ru.nsu.fit.endpoint.service.database.data.ServicePlan;
import ru.nsu.fit.endpoint.service.database.data.User;

//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

@Path("/user")
public class UserManagementService {



    @POST
    @Path("/add")
    public Response addUser(
            @FormParam("firstname") String firstname,
            @FormParam("lastname") String lastname,
            @FormParam("email") String email,
            @FormParam("login") String login,
            @FormParam("pass") String pass
    ) {

        try {
            User user = new User(firstname, lastname, email, login, pass, User.UserRole.USER);
            db_connect.DBadd(user.getFirstName(), user.getLastName(), user.getEmail(), user.getLogin(), user.getUserRole().getRoleName());
            return Response.status(200).entity(Style.Bootstrap + "Success" + Style.Js).build();
        } catch (Exception e) {
            return Response.status(400).entity(e.getMessage()).build();
        }
    }

    @POST
    @Path("/log_into")
    public Response loginUser(
            @FormParam("login") String login,
            @FormParam("pass") String pass
    ) {

        if(db_connect.DBcheckREG(login, pass)){

            return Response.status(200)
                    //.entity("Login Successful!" + "<script>window.location.replace(\"service_plan\");</script>")
                    .entity(Style.Bootstrap + "Login Successful!" +
                            "<script>window.location.replace('http://localhost:8081/rest/service_plan/list?login=" +
                            login +"');</script>"
                            + Style.Js)
                    .build();

        } else return Response.status(403)
                .entity(Style.Bootstrap + "Login Unsuccessful!" + Style.Js)
                .build();

    }

}