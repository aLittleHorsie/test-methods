package ru.nsu.fit.endpoint.rest;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

/**
 * Created by tolyas on 9/29/16.
 */
@Path("/subscription")
public class SubscriptionService {

    @Path("/subscribe")

    @GET
    public Response get(
            @QueryParam("user") String user,
            @QueryParam("sub_id") String sub_id
    ){
        return Response.status(200).entity("Login: " + user + " || SubID: " +sub_id).build();
    }

}
