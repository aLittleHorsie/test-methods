<%--
  Created by IntelliJ IDEA.
  User: aleksandrkucherov
  Date: 29.09.16
  Time: 20:25
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
          crossorigin="anonymous">
    <style>
        .form-horizontal{
            width: 50%;
            margin-top: 10%;
            margin-left: 25%;
        }
    </style>
</head>
<body>

<form action="rest/user/log_into" class="form-horizontal" role="form" method="post">
    <h3 align="center">Login</h3>

    <div class="form-group">
        <label for="inputLogin" class="col-sm-2 control-label">Login</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="inputLogin" name="login" placeholder="Login"  required>
        </div>
    </div>
    <div class="form-group">
        <label for="inputPassword" class="col-sm-2 control-label">Password</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" id="inputPassword" name="pass" placeholder="Password"  required>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Log into</button>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <div class="checkbox">
                <label>
                    <p>Or if u're not registered -> <a href="reg.jsp">Register</a> </p>
                </label>
            </div>
        </div>
    </div>

</form>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</body>
</html>
